#include "prompt.h"
#include "headers.h"
#include "echo.h"
#include "cd.h"
#include "ls.h"
#include "exec.h"
#include "pinfo.h"
#include "history.h"
#include "background.h"
#include "redirection.h"
#include "piping.h"
#include <assert.h>
#include "env.h"
void execute_pipe(int in,int out,char* main_comm,char* og_dir)
{
        dup2(in,STDIN_FILENO);
        close(in);
        dup2(out,STDOUT_FILENO);
        close(out);
        
        int redirection_flag=redirection(main_comm,og_dir);
        if (redirection_flag==1)
            return;
        char* temp=(char*)malloc(100*sizeof(char));
         strcpy(temp,main_comm);
   // printf("||%s||",main_comm);
        char* first_word=strtok(temp," \t");

        if(strcmp(first_word,"cd")==0){    
        		cd(og_dir,main_comm);    
        	}
        	else if(strcmp(first_word,"echo")==0){
        		echo(main_comm);
        	}
        	else if(strcmp(first_word,"pwd")==0)
        	{
        		char cur_dir[200];
        		getcwd(cur_dir,sizeof(cur_dir));
        		printf("%s\n",cur_dir);
        	}
        	else if( strcmp(first_word,"ls")==0)
        	{   
        		lis(og_dir,main_comm);
        	}    
        	else if (strcmp (first_word,"pinfo")==0)
        	{
        		pinfo(main_comm);
        	}
        	else if(strcmp (first_word, "history")==0)
        	{
        		dis_history(main_comm);
        	}
            else if(strcmp (first_word,"setenv")==0)
            {
                set_env(main_comm);
            }
            else if(strcmp (first_word,"unsetenv")==0)
            {
                unset_env(main_comm);
            }
             else if (strcmp (first_word,"quit")==0)
            {
                kill(og_pid,SIGKILL);
            }
            else if (strcmp (first_word,"overkill")==0)
            {
                overkill();
            }
            else if (strcmp (first_word,"jobs")==0)
            {
                jobs();
            }
            else if (strcmp (first_word,"kjob")==0)
            {
                kjob(main_comm);
            }
            else if (strcmp (first_word,"fg")==0)
            {
                fg(main_comm);
            }
            else if (strcmp (first_word,"bg")==0)
            {
                bg(main_comm);
            }
            else
        	{
        		int pos=strlen(main_comm)-1;
        		while(main_comm[pos]==' ')
        			pos--;
        		if(main_comm[pos]!='&')
        			exec(main_comm);
        		else
        			background(main_comm);
        	} 
}

int piping(char *command,char* og_dir)
{
   // printf("%s\n",command);
    int pipe_count=0;
    int len=strlen(command);
    for(int i=0;i<len;i++)
    {
        if(command[i]=='|')
            pipe_count++;
    }
   // printf("|%d| ",pipe_count);
    
    if(pipe_count==0)
        return 0;
    
    int i=0;
    int in=dup(STDIN_FILENO),og_in=dup(STDIN_FILENO),og_out=dup(STDOUT_FILENO);
    char inputs[200][200];
    int input_count=0;
    char* token = strtok(command,"|"); // segregating by ;
        
        while (token!=NULL)                            
        {
        	strcpy(inputs[input_count],token);
        	input_count++;
        	token=strtok(NULL,"|");        	
        }

    for(;i<input_count-1;i++)
    {
        int fd[2];
        if(pipe(fd)==-1){
            printf("Piping Failed\n");
            return 1;
        }
        int pid=fork();
        if (pid==0){
            close(fd[0]);
        execute_pipe(in,fd[1],inputs[i],og_dir);
        exit(0);
        }
       
       else{
           wait(NULL);
           assert(pid>0);
           close(fd[1]);
           close(in);
           in=fd[0];
       }
    }

    if (in!=0)
        dup2(in,STDIN_FILENO);
    
    int redirection_flag=redirection(inputs[i],og_dir);
    if(redirection_flag==1){
        dup2(og_in,STDIN_FILENO);
        dup2(og_out,STDOUT_FILENO);
        return 1;

    }
    dup2(og_out,STDOUT_FILENO);
    char* temp=(char*)malloc(100*sizeof(char));
         strcpy(temp,inputs[i]);
   // printf("||%s||",main_comm);
        char* first_word=strtok(temp," \t");
    
    if(strcmp(first_word,"cd")==0){    
        		cd(og_dir,inputs[i]);    
        	}
        	else if(strcmp(first_word,"echo")==0){
        		echo(inputs[i]);
        	}
        	else if(strcmp(first_word,"pwd")==0)
        	{
        		char cur_dir[200];
        		getcwd(cur_dir,sizeof(cur_dir));
        		printf("%s\n",cur_dir);
        	}
        	else if( strcmp(first_word,"ls")==0)
        	{
        		lis(og_dir,inputs[i]);
        	}    
        	else if (strcmp (first_word,"pinfo")==0)
        	{
        		pinfo(inputs[i]);
        	}
        	else if(strcmp (first_word, "history")==0)
        	{
        		dis_history(inputs[i]);
        	}
            else if(strcmp(first_word,"setenv")==0)
            {
                set_env(inputs[i]);
            }
            else if(strcmp(first_word,"unsetenv")==0)
            {
                unset_env(inputs[i]);
            }
             else if (strcmp (first_word,"quit")==0)
            {
                kill(og_pid,SIGKILL);
            }
             else if (strcmp (first_word,"overkill")==0)
            {
                overkill();
            }
            else if (strcmp (first_word,"jobs")==0)
            {
                jobs();
            }
            else if (strcmp (first_word,"kjob")==0)
            {
                kjob(inputs[i]);
            }
            else if (strcmp (first_word,"fg")==0)
            {
                fg(inputs[i]);
            }
             else if (strcmp (first_word,"bg")==0)
            {
                bg(inputs[i]);
            }
            else
        	{
        		int pos=strlen(inputs[i])-1;
        		while(inputs[i][pos]==' ')
        			pos--;
        		if(inputs[i][pos]!='&')
        			exec(inputs[i]);
        		else
        			background(inputs[i]);
        	}

    dup2(og_in,STDIN_FILENO);
    dup2(og_out,STDOUT_FILENO);
    return pipe_count;

}