#include "ls.h"
#include "headers.h"

void lis(char og_dir[100], char *command)
{
	char inputs[200][200];
	int input_count=0,is_a=0,is_l=0,arg_c=0;
	char *token = strtok(command," \t");
    while (token!=NULL)
    {
    	strcpy(inputs[input_count],token);
        	input_count++;
        	token=strtok(NULL," \t");  
    }
   for (int i=1;i<input_count;i++)
    {
    	if(strcmp(inputs[i],"-l")==0)         	
    		is_l=1;
  
    	if(strcmp(inputs[i],"-a")==0)
    		is_a=1;

    	if( strcmp("-la",inputs[i])==0)
    	{
    		is_l=1;
    		is_a=1;
    	}
    	if( strcmp("-al",inputs[i])==0)
    	{
    		is_l=1;
    		is_a=1;
    	}

    	if(inputs[i][0]!='-'){
    		arg_c++;        // counting number of arguments        // checked if -l and -a were present
    	}
    }
     for (int i=0;i<input_count;i++)
    {
      if(inputs[i][0]=='-' && strcmp(inputs[i],"-la")!=0 && strcmp(inputs[i],"-a")!=0 && strcmp(inputs[i],"-al")!=0 && strcmp(inputs[i],"-l")!=0)
        printf("ls: invalid option -- '%s'\n",inputs[i] );
    } 
 printf("\n");

    if (arg_c==0)
    {
    	struct dirent **namelist;
       int n;
       n = scandir(".", &namelist, NULL, alphasort); // getting all the contained files
       if (n < 0)
           perror("scandir");
      
       else {
       
          while(n--){
          		char f_name[200];
          		
          		strcpy(f_name,namelist[n]->d_name);
          		if(is_a==0 && f_name[0]=='.')
           		     continue;     
                                       //ignoring hidden files
               struct stat tbd;
               if(is_l==0)
               {
               	printf("%s\n",f_name);
               	continue;
               }
               int s=stat(namelist[n]->d_name,&tbd);
               if(s<0)
               {
               	perror(namelist[n]->d_name);
               	continue;
               }
               struct group *grp;
				struct passwd *pwd;
				grp = getgrgid(tbd.st_gid);         // getting group id
			

				pwd = getpwuid(tbd.st_uid);      //username id
				
				printf( (S_ISDIR(tbd.st_mode)) ? "d" : "-");
    			printf( (tbd.st_mode & S_IRUSR) ? "r" : "-");
    			printf( (tbd.st_mode & S_IWUSR) ? "w" : "-");
    			printf( (tbd.st_mode & S_IXUSR) ? "x" : "-");
    			printf( (tbd.st_mode & S_IRGRP) ? "r" : "-");
    			printf( (tbd.st_mode & S_IWGRP) ? "w" : "-");
    			printf( (tbd.st_mode & S_IXGRP) ? "x" : "-");
    			printf( (tbd.st_mode & S_IROTH) ? "r" : "-");
    			printf( (tbd.st_mode & S_IWOTH) ? "w" : "-");
    			printf( (tbd.st_mode & S_IXOTH) ? "x" : "-");
    			printf(" ");
               free(namelist[n]);

               char time[20];
               struct tm* timeinfo;
               timeinfo = localtime(&(tbd.st_mtime));
               strftime(time,20,"%b %2d %H:%M",timeinfo);

               printf("%3ld %20s %20s %20ld %s %-35s\n",tbd.st_nlink,pwd->pw_name,grp->gr_name,tbd.st_size,time,f_name);
           }
           free(namelist);
       }
       return;
    }

   for(int i=1;i<input_count ;i++)
    {
    	if( inputs[i][0]=='-')
    		continue;
    	char* final_path;
        final_path=(char*) malloc(300*sizeof(char)); 
    	if('~'==inputs[i][0]) // case for cd ~/xyz
      {
    
         // final path with ~ replaced by absolute path (hopefully)
        char rest[200];
        int j=0;
        for (int k=1;k<(int)strlen(inputs[i]);k++)      // copyin xyz
            rest[j++]=inputs[i][k];
       rest[j]='\0';
        sprintf(final_path,"%s%s",og_dir,rest);		//finished product
        strcpy(inputs[i],final_path);

      }
       struct dirent **namelist;
       int n;
       char cur_dir[200];
       getcwd(cur_dir,sizeof(cur_dir));
       int did=chdir(inputs[i]);
       if(did<0)
       {
       	perror(inputs[i]);
       	continue;
       }
       n = scandir(".", &namelist, NULL, alphasort); // getting all the contained files
       if (n < 0){
           perror("scandir");
           //printf("Stuck in n<0\n");
       }

       else {
       	printf("\n%s:\n",inputs[i] );
     //  printf("Crossed into else\n");
          while(n--){
          		//printf("%s\n", namelist[n]->d_name);
          		char f_name[200];
          		
          		strcpy(f_name,namelist[n]->d_name);
          		if(is_a==0 && f_name[0]=='.')
           		continue;                              //ignoring hidden files
               //strcpy(f_name[n],namelist[n]->d_name); 
              // printf("|%s|\n",f_name);
               struct stat tbd;
               if(is_l==0)
               {
               	printf("%s\n",f_name);
               	continue;
               }
              // printf("Crossed is_l\n");
               int s=stat(namelist[n]->d_name,&tbd);
               struct group *grp;
				struct passwd *pwd;
				if(s<0)
               {
               	perror(namelist[n]->d_name);
               	continue;
               }
              // printf("s>0\n");
				grp = getgrgid(tbd.st_gid);         // getting group id
				//printf("group: %s\n", grp->gr_name);

				pwd = getpwuid(tbd.st_uid);      //username id
				//printf("username: %s\n", pwd->pw_name);
				//if(arg_c>1)
				//{
					//printf("\n%s\n",inputs[i]);
				//}
				printf( (S_ISDIR(tbd.st_mode)) ? "d" : "-");
    			printf( (tbd.st_mode & S_IRUSR) ? "r" : "-");
    			printf( (tbd.st_mode & S_IWUSR) ? "w" : "-");
    			printf( (tbd.st_mode & S_IXUSR) ? "x" : "-");
    			printf( (tbd.st_mode & S_IRGRP) ? "r" : "-");
    			printf( (tbd.st_mode & S_IWGRP) ? "w" : "-");
    			printf( (tbd.st_mode & S_IXGRP) ? "x" : "-");
    			printf( (tbd.st_mode & S_IROTH) ? "r" : "-");
    			printf( (tbd.st_mode & S_IWOTH) ? "w" : "-");
    			printf( (tbd.st_mode & S_IXOTH) ? "x" : "-");
    			printf(" ");
               free(namelist[n]);

               char time[20];
               struct tm* timeinfo;
               timeinfo = localtime(&(tbd.st_mtime));
               strftime(time,20,"%b %2d %H:%M",timeinfo);

              printf("%3ld %20s %20s %20ld %s %-35s\n",tbd.st_nlink,pwd->pw_name,grp->gr_name,tbd.st_size,time,f_name);
           }
           free(namelist);
       }

chdir(cur_dir);
    }

}
