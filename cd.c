#include "prompt.h"
#include "headers.h"

void cd(char* og_dir, char* command)
{
	char inputs[200][200];
    int input_count=0,stat;
    char *token = strtok(command," \t");
    while (token!=NULL)
    {
    	strcpy(inputs[input_count],token);
        	//printf("tok|%s|\n",inputs[input_count]);
        	input_count++;
        	token=strtok(NULL," \t");  
    }
   
  

    if (input_count==1 || (strcmp("~",inputs[1])==0 && (int)strlen(inputs[1])==1))// case for cd and cd ~
            stat=chdir(og_dir); 
            
                     
    else if (inputs[1][0]=='-')
    {
        printf("%s\n",lwd);
        char* rec=(char*)malloc(100*sizeof(char));
        sprintf(rec,"cd %s",lwd);
        cd(og_dir,rec);
        free(rec);
    }
    
    else if('~'==inputs[1][0]) // case for cd ~/xyz
    {
        char* final_path;
        final_path=(char*) malloc(300*sizeof(char));  // final path with ~ replaced by absolute path (hopefully)
        char rest[200];
        int j=0;
        for (int i=1;i<(int)strlen(inputs[1]);i++)      // copyin xyz
            rest[j++]=inputs[1][i];
       rest[j]='\0';
        sprintf(final_path,"%s%s",og_dir,rest);
        
        stat=chdir(final_path);
    }

    else
        stat=chdir(inputs[1]);

    if(stat<0){
        printf("%s\n",inputs[1] );
    	perror(inputs[1]);
  }


}