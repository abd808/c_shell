#include "prompt.h"
#include "headers.h"
#include "echo.h"
#include "cd.h"
#include "ls.h"
#include "exec.h"
#include "pinfo.h"
#include "history.h"
#include "background.h"
#include "redirection.h"
#include "env.h"
int redirection(char* command,char* og_dir)
{
    int is_input=0,is_output=0,input_pos=-2,output_pos=-2,in_attached=0,out_attached=0;  //attached tells me if its of the form >xyx

    for (int i=0;command[i]!='\0';i++)
    {
        if (command[i]=='<'){
            is_input=1;
            //input_pos=i;
        }
        if (command[i]=='>')
        {
            if(i!=0 && command[i-1]=='>')
                is_output=2;
            else
            {
                is_output=1;
            }   
           // output_pos=i;         
        }
    }
     if (is_output==0 && is_input==0)
        return 0;

    int og_in=dup(STDIN_FILENO);
    int og_out=dup(STDOUT_FILENO); // saving old fd's for input and output
    int new_in=dup(STDIN_FILENO);
    int new_out=dup(STDOUT_FILENO);
   
    char* input_file;
    char* output_file;
    char* main_comm;
    char* temp;
    temp=(char*)malloc(200*sizeof(char));
    input_file=(char*)malloc(200*sizeof(char));
    output_file=(char*)malloc(200*sizeof(char));
    main_comm=(char*)malloc(200*sizeof(char));
    char* help_me=main_comm;
    strcpy(temp,command);

    char inputs[200][200];
    int input_count=0;
    char *token = strtok(temp," \t");
    while (token!=NULL)
    {
    	strcpy(inputs[input_count],token);
        	//printf("tok|%s|\n",inputs[input_count]);
            if(inputs[input_count][0]=='<')
            {
                if(strlen(inputs[input_count])>1)
                    in_attached=1;
                input_pos=input_count;
            }
            if(is_output==2)
            {
                if(inputs[input_count][0]=='>'&& inputs[input_count][1]=='>')
                {
                    if(strlen(inputs[input_count])>2)
                        out_attached=1;
                    output_pos=input_count;
                }
            }
            if(is_output==1)
            {
                if(inputs[input_count][0]=='>')
                {
                    if(strlen(inputs[input_count])>1)
                        out_attached=1;
                    output_pos=input_count;
                }
            }
        	input_count++;
        	token=strtok(NULL," \t");  
    }

   // printf("%d %d %d %d\n",in_attached,out_attached,input_pos,output_pos);    


    // fishing out input and output paths
    if (is_input)
    {
        if(in_attached)
        {
            int len=strlen(inputs[input_pos]);
            int k=0;
            for(int i=1;i<len;i++)
                input_file[k++]=inputs[input_pos][i];
            input_file[k]='\0';
        }
        else
        {
            strcpy(input_file,inputs[input_pos+1]);
        }
      //  printf("|%s|\n",input_file);
    }

    if (is_output!=0)
    {
        if(out_attached)
        {   
            int len=strlen(inputs[output_pos]);
            int k=0,i;
            i=is_output;
                for(;i<len;i++)
                    output_file[k++]=inputs[output_pos][i];
            output_file[k]='\0';   
        }
        else
        {
            strcpy(output_file,inputs[output_pos+1]);
        }
      //  printf("|%s|\n",output_file);
    }

    //extracting main command

    if(in_attached==0 && out_attached==0)
    {
        for(int i=0;i<input_count;i++)
            if(i!=input_pos && i!=input_pos+1 && i!=output_pos && i!=output_pos+1){
                inputs[i][strlen(inputs[i])]='\0';
               help_me+= sprintf(help_me,"%s ",inputs[i]);
            }
    }

    if(in_attached==0 && out_attached==1)
    {
        for(int i=0;i<input_count;i++)
            if(i!=input_pos && i!=input_pos+1 && i!=output_pos){
                inputs[i][strlen(inputs[i])]='\0';
             //   printf("@%s@\n",inputs[i]);
               help_me+= sprintf(help_me,"%s ",inputs[i]);
            }
    }

    if(in_attached==1 && out_attached==0)
    {
        for(int i=0;i<input_count;i++)
            if(i!=input_pos && i!=output_pos && i!=output_pos+1){
                inputs[i][strlen(inputs[i])]='\0';
               help_me+= sprintf(help_me,"%s ",inputs[i]);
            }
    }
    
    if(in_attached==1 && out_attached==1)
    {
        for(int i=0;i<input_count;i++)
            if(i!=input_pos && i!=output_pos){
                inputs[i][strlen(inputs[i])]='\0';
               help_me+= sprintf(help_me,"%s ",inputs[i]);
            }
    }

   // printf("why |%s|\n",main_comm);
    int f_in=-1,f_out=-1;
    if(is_input)
    {
         f_in=open(input_file,O_RDONLY);
        if(f_in<0)
            perror(input_file);
        else
        {
            dup2(f_in,new_in);
        }        
    }
    if(is_output!=0)
    {
        
        if(is_output==1)
            f_out=open(output_file, O_WRONLY | O_TRUNC | O_CREAT, 0644);
        if(is_output==2)
            f_out=open(output_file, O_APPEND | O_WRONLY | O_CREAT, 0644);
        if(f_out<0)
            perror(output_file);
        else
            dup2(f_out,new_out);
    }

    free(temp);
    temp=(char*)malloc(100*sizeof(char));
    strcpy(temp,main_comm);
   // printf("||%s||",main_comm);
    char* first_word=strtok(temp," \t");
   // printf("|%s|\n",first_word);

    dup2(new_out,STDOUT_FILENO);
    dup2(new_in,STDIN_FILENO);  //changed paths
    int pid=fork();
    if(pid==0){
       // printf("%s %s\n",first_word,main_comm);
        	if(strcmp(first_word,"cd")==0){    
        		cd(og_dir,main_comm);    
        	}
        	else if(strcmp(first_word,"echo")==0){
        		echo(main_comm);
        	}
        	else if(strcmp(first_word,"pwd")==0)
        	{
        		char cur_dir[200];
        		getcwd(cur_dir,sizeof(cur_dir));
        		printf("%s\n",cur_dir);
        	}
        	else if( strcmp(first_word,"ls")==0)
        	{   
        		lis(og_dir,main_comm);
        	}    
        	else if (strcmp (first_word,"pinfo")==0)
        	{
        		pinfo(main_comm);
        	}
        	else if(strcmp (first_word, "history")==0)
        	{
        		dis_history(main_comm);
        	}
            else if(strcmp (first_word,"setenv")==0)
            {
                set_env(main_comm);
            }
             else if(strcmp (first_word,"unsetenv")==0)
            {
                unset_env(main_comm);
            }
            else if (strcmp (first_word,"quit")==0)
            {
                kill(og_pid,SIGKILL);
            }
            else if (strcmp (first_word,"overkill")==0)
            {
                overkill();
            }
            else if (strcmp (first_word,"jobs")==0)
            {
                jobs();
            }
            else if (strcmp (first_word,"kjob")==0)
            {
                kjob(main_comm);
            }
             else if (strcmp (first_word,"fg")==0)
            {
                fg(main_comm);
            }
             else if (strcmp (first_word,"bg")==0)
            {
                bg(main_comm);
            }
            else
        	{
        		int pos=strlen(main_comm)-1;
        		while(main_comm[pos]==' ')
        			pos--;
        		if(main_comm[pos]!='&')
        			exec(main_comm);
        		else
        			background(main_comm);
        	} 
        exit(0);
    }// child fork ends
    else{
        wait(NULL);
    dup2(og_in,STDIN_FILENO);
    dup2(og_out,STDOUT_FILENO);
    if(f_in>=0)
        close(f_in);
    if(f_out>=0)
        close(f_out);
    free(input_file);
    free(output_file);
    free(temp);
    free(main_comm);
    free(token);
    if(is_input==1 || is_output!=0)
        return 1;
    }
}