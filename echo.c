#include "echo.h"
#include "headers.h"

void echo(char* command)
{
	char inputs[200][200];
    int input_count=0;
    char *token = strtok(command," \t");
    while (token!=NULL)
    {
    	strcpy(inputs[input_count],token);
        	//printf("tok|%s|\n",inputs[input_count]);
        	input_count++;
        	token=strtok(NULL," \t");  
    }
    if(input_count==1) // case for just echo
    {
    	printf("\n");
    	return;
    }
    for(int i=1;i<input_count;i++)
    {
    	
    		int len=strlen(inputs[i]);
    		for(int j=0;j<len;j++)
    			//if(inputs[i][j]!='"')
    				printf("%c",inputs[i][j]);
    		printf(" ");
    	
    }
    printf("\n");
}
