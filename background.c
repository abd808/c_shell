#include "headers.h"
#include "background.h"



/*void process_over()
{
    for (int i=0;i<p_count;i++)
    {
        if (processes[i].x==0 && kill(processes[i].pid,0)!=0)
        {
            printf("\033[01;33m");
            printf("\n%s with pid %d exited successfully\n\n", processes[i].name,processes[i].pid);
            printf("\033[0m");
            processes[i].x=1;
            //printf("wot");
            printf("<Press Enter to Continue>\n");
           // fflush(stdout);
            prompt();
        }
    }

}*/

void overkill()
{
    for (int i=0;i<p_count;i++)
    {
        if(processes[i].x==0){
            //processes[i].x=1;
            kill(processes[i].pid,SIGKILL);
        }
    }
}

void jobs()
{
    int k=1;
    for (int i=0;i<p_count;i++)
    {
        if(processes[i].x==1)
            continue;
        int pid=processes[i].pid;
        char filename[100];
        int unused;
        char buff[100],message[100],state;

        sprintf(filename,"/proc/%d/stat",pid);
        FILE *f=fopen(filename,"r");
        fscanf(f,"%d %s %c ",&unused,buff,&state);

        if(state=='T')
            strcpy(message,"Stopped");
        else
        {
            strcpy(message,"Running");
        }
        
        printf("[%d] %s %s [%d]\n",k,message,processes[i].name,pid);
        k++;

    }
}

void sig_handler(int signum)
{
    int status;
    signal(SIGCHLD,sig_handler);
    int id=waitpid(-1,&status,WNOHANG);
    for (int i=0;i<p_count;i++)
    {
       // printf(" %d %d %d %s\n",id,processes[i].pid,processes[i].x,processes[i].name);
        if((processes[i].pid==id) && (processes[i].x==0))
        {
           // printf("|HI|\n");
            printf("\033[01;33m");
            if (WIFEXITED(status))
                printf("\n%s with pid %d exited normally\n\n", processes[i].name,processes[i].pid);
            if (WIFSIGNALED(status))
                printf("\n%s with pid %d exited abnormally with exit code %d \n\n", processes[i].name,processes[i].pid,WTERMSIG(status));
            printf("\033[0m");
            processes[i].x=1;
           // printf("<Press Enter to Continue>\n");
           prompt(og_dir);
            fflush(stdout);
        }
    }
    //signal(SIGCHLD,sig_handler);
    
}

void background(char* command)
{
    //printf("Entered exec&\n");
    char* inputs[200];
    int input_count=0;
    char *token = strtok(command," \t");
    while (token!=NULL)
    {
        inputs[input_count]=(char*) malloc(200*sizeof(char)); 
        strcpy(inputs[input_count],token);
            //printf("tok|%s|\n",inputs[input_count]);
            input_count++;
            token=strtok(NULL," \t");         // separating by space
    }
   // inputs[input_count]=(char*) malloc(200*sizeof(char)); 
    inputs[input_count-1]=NULL;
   

    int fork_val=fork();   //first fork
    
    if(fork_val==-1)
    {
        printf("%s\n","Fork Failed" );
        return;
    }
    else if (fork_val==0){
        if(fork_val==0){
            setpgrp();
        if(execvp(inputs[0],inputs)<0)
            printf("%s\n","Could Not Execute Command" );
        }
        exit(0);
    }
    else{        
        printf("%s [%d]\n",inputs[0],fork_val);
        processes[p_count].pid=fork_val;
        strcpy(processes[p_count].name,inputs[0]);
        p_count++;
        signal(SIGCHLD,sig_handler);
        return;
    } 

}