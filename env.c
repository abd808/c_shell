#include "headers.h"
#include "env.h"

void set_env(char *command)
{   
    char inputs[100][100];
    int input_count=0;
    char *token = strtok(command," \t");
    while (token!=NULL)
    {
    	strcpy(inputs[input_count],token);
        	//printf("tok|%s|\n",inputs[input_count]);
        	input_count++;
        	token=strtok(NULL," \t");  
    } 

    if(input_count==1 || input_count>3)
    {
        printf("Incorrect Number of Arguments\n");
        return;
    }

    char* to_set=(char*)malloc(100*sizeof(char));
    if (input_count==3)
        to_set=inputs[2];
    else
    {
        to_set[0]=' ';
        to_set[1]='\0';
    }

    int check=setenv(inputs[1],to_set,1);
    if(check<0)
        perror("setenv: ");
} 

void unset_env(char *command)
{
    char inputs[100][100];
    int input_count=0;
    char *token = strtok(command," \t");
    while (token!=NULL)
    {
    	strcpy(inputs[input_count],token);
        	//printf("tok|%s|\n",inputs[input_count]);
        	input_count++;
        	token=strtok(NULL," \t");  
    } 

    if(input_count!=2)
    {
        printf("Incorrect Number of Arguments\n");
        return;
    }

    int check=unsetenv(inputs[1]);
    if (check<0)
        perror("unsetenv: ");

}