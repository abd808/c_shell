#include "headers.h"

void fg(char* command)
{
    char inputs[200][200];
	int input_count=0,is_a=0,is_l=0,arg_c=0;
	char *token = strtok(command," \t");
    while (token!=NULL)
    {
    	strcpy(inputs[input_count],token);
        	input_count++;
        	token=strtok(NULL," \t");  
    }

    if (input_count!=2)
    {
        printf("Incorrect Number of Arguments\n");
        return;
    }

    int k=1;
    int j_no=atoi(inputs[1]);
    int g_no=getpgrp();
    for(int i=0;i<p_count;i++)
    {
        if(processes[i].x!=1)
        {
            if(j_no==k)
            {

                signal(SIGTTIN,SIG_IGN);
                signal(SIGTTOU,SIG_IGN);
                tcsetpgrp(STDIN_FILENO,processes[i].pid);
                fore_id=processes[i].pid;
                kill(fore_id,SIGCONT);
                waitpid(-1,NULL,WUNTRACED);
                processes[i].x=1;
                tcsetpgrp(STDIN_FILENO,g_no);
                signal(SIGTTIN,SIG_DFL);
                signal(SIGTTOU,SIG_DFL);
                return;
            }
            k++;
        }
    }

    printf("No job with given job number found\n");
}