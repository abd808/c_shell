#include "headers.h"
#include "exec.h"

void exec(char* command)
{
	char* inputs[200];
    int input_count=0;
    char *token = strtok(command," \t");
    while (token!=NULL)
    {
    	inputs[input_count]=(char*) malloc(200*sizeof(char)); 
    	strcpy(inputs[input_count],token);
        	input_count++;
        	token=strtok(NULL," \t");         // separating by space
    }
    inputs[input_count]=(char*) malloc(200*sizeof(char)); 
    inputs[input_count]=NULL;
    int fork_val=fork();
    fore_id=fork_val;
    strcpy(fore_name,inputs[0]);
    if(fork_val==-1)
    {
    	printf("%s\n","Fork Failed" );
    	return;
    }
    else if (fork_val==0){
    	if(execvp(inputs[0],inputs)<0)
    		printf("%s\n","Could Not Execute Command" );
    	exit(0);
    }
    else{
    	wait(NULL);
    	return;
    } 
   
}