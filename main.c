#include "prompt.h"
#include "headers.h"
#include "echo.h"
#include "cd.h"
#include "ls.h"
#include "exec.h"
#include "pinfo.h"
#include "history.h"
#include "background.h"
#include "redirection.h"
#include "piping.h"
#include "env.h"
 


void ctrl_z(int signum){
	int pid = getpid();
	if(pid == og_pid)
	{
		return;
	}
	if(fore_id < 0)
		return;
	int ff = 0;
	for(int ii = 0; ii < p_count; ii++){
		if(processes[ii].pid == fore_id){
			ff = 1;
			break;
		}
	}
	if(ff == 0){
			strcpy(processes[p_count].name, fore_name);
			processes[p_count].pid = fore_id;
			p_count++;
	}
	if(fore_id!=og_pid){
		kill(fore_id, SIGSTOP);
		fore_id = 0;
		fflush(stdout);
	}

	signal(SIGTSTP, ctrl_z);
		
}

void ctrl_c(int signum)
{
	int pid=getpid();
	if(pid!=og_pid)
	return;

	if (fore_id==0)
	{
		return;
	}
	if(fore_id==-1 || fore_id==0)
	{
		fflush(stdout);
		prompt();
		return;
	}

	if(pid==og_pid && (fore_id==-1 || fore_id==0))
	{
		prompt();
		fflush(stdout);
		return;
	}

	if(fore_id!=-1)
		kill(fore_id,SIGINT);
	signal(SIGINT,ctrl_c);
}

int main()
{
	lwd_flag=0;
	getcwd(og_dir, sizeof(og_dir));
	load_history(og_dir);
	p_count=0;
	og_pid=getpid(); 

	signal(SIGCHLD,sig_handler);
	 signal(SIGINT, ctrl_c);
	signal(SIGTSTP,ctrl_z);
    while (1)
    {
    	
    	char* input;
    	char inputs[200][200];
    	int input_count=0;
    	size_t size =200;
    	input=(char*) malloc(200*sizeof(char));
        prompt(og_dir);									// calling prompt
        if(getline(&input,&size,stdin)==-1)
			kill(og_pid,SIGKILL);
		
        char* token = strtok(input,";"); // segregating by ;
        
        while (token!=NULL)                            
        {
        	strcpy(inputs[input_count],token);
        	input_count++;
        	token=strtok(NULL,";");        	
        }
        if(inputs[input_count-1][0]=='\n') // getting rid of the last \n chatacter input if its taken as a separate word
        	input_count--;
        else
        	inputs[input_count-1][(int)strlen(inputs[input_count-1])-1]='\0'; // removing last \n character joined at the end of last word
			
        for (int i=0;i<input_count;i++)
        {				
			//printf("%s\n",inputs[i]);
			//printf("%ld\n",strlen(inputs[i]));
        	char temp[strlen(inputs[i]+5)];
        	 int input_count_no_change=input_count;
        	 int i_no_change=i;
			
				int len=strlen(inputs[i]);
				int wtf;
				for ( wtf=0;wtf<len;wtf++)
				temp[wtf]=inputs[i][wtf];
				temp[wtf]='\0';
				//printf("temp: %s",temp);
				 
        	i=i_no_change;
        	input_count=input_count_no_change;
        	char* command= strtok(temp," \t");                           // taking first word 
			//printf("|%s|",command);
        	update_history(inputs[i]);
			
			int piping_flag=piping(inputs[i],og_dir);
			if(piping_flag!=0)
				continue;

			int redirection_flag=redirection(inputs[i],og_dir);				// checking for redirection
			if(redirection_flag==1)
				continue;
			//printf("continung\n");

		//	printf("%s",command);
			
        	if(strcmp(command,"cd")==0){    
        		cd(og_dir,inputs[i]);    
        	}
        	else if(strcmp(command,"echo")==0){
        		echo(inputs[i]);
        	}
        	else if(strcmp(command,"pwd")==0)
        	{
        		char cur_dir[200];
        		getcwd(cur_dir,sizeof(cur_dir));
        		printf("%s\n",cur_dir);
        	}
        	else if( strcmp(command,"ls")==0)
        	{
        		lis(og_dir,inputs[i]);
        	}    
        	else if (strcmp (command,"pinfo")==0)
        	{
				//printf("enter pinfo");
        		pinfo(inputs[i]);
        	}
        	else if(strcmp (command, "history")==0)
        	{
        		dis_history(inputs[i]);
        	}
			else if(strcmp (command,"setenv")==0)
			{
				set_env(inputs[i]);
			}
			else if(strcmp (command,"unsetenv")==0)
			{
				unset_env(inputs[i]);
			}
			else if (strcmp (command,"quit")==0)
			{
				kill(og_pid,SIGKILL);
			}
			else if (strcmp (command,"overkill")==0)
			{
				overkill();
			}
			else if (strcmp (command,"jobs")==0)
			{
				jobs();
			}
			else if (strcmp (command,"kjob")==0)
			{
				kjob(inputs[i]);
			}
			else if (strcmp (command,"fg")==0)
			{
				fg(inputs[i]);
			}
			else if (strcmp (command,"bg")==0)
			{
				bg(inputs[i]);
			}
            else
        	{
        		int pos=strlen(inputs[i])-1;
        		while(inputs[i][pos]==' ')
        			pos--;
        		if(inputs[i][pos]!='&')
        			exec(inputs[i]);
        		else
        			background(inputs[i]);
        	}

        	input_count=input_count_no_change;
        }
    }
}
