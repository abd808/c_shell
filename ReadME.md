## C Implementation of Linux Shell

This implementation aims to mimic the functionality of the Linux Terminal , and is implemented with a mix of user implemented functions and execvp calls, but provides almost all the functionalities of a Linux terminal.

The requirements docs , or the specs for the assignment according to which this project is coded are included in the repository

Note that **not** all Bonus specs have been met.

**Instructions**

- Enter the folder with the downloaded executable.
- Make sure a file `history.txt` is present in that folder
- Run `./abd_shell`
- If executable is not present, code is complied using simple   `make` command.

**Files and their Functionalities**

- `main.c`: 
	- takes in input and parses out the semicolons, sends the input to appropriate function based on the command
	- contains functionality for `pwd` command
	- contains all the signal handler functions for ctrl_[x] type commands

- `prompt.c` : deals with displaying the shell prompt 

- `ls.c` : Implements ls command, prints names and/or details in given directories. Accepts `-l`, `-a` flags, and any combinations of these two flags.

- `echo.c` : Implements echo command, prints space separated arguments removing excess whitespace.

- `history.c` : On command `history` prints the last 10 commands entered into the shell, and allows `history x` where x<=20 (stores a maximum of 20 previous commands).

- `cd.c` : Implements cd command, lets you change current working directory. Supports `.`,`..`,`~` and `-` flags.

- `pinfo.c` - Displays the details of process corresponding to given process ID, by default displays pinfo of current(abd_shell) process.

- `background.c` :
    - handles background processes run using `command &`and displays exit message for them 
    - handles `overkill`: kills all background processes
    - handles `jobs`: prints a list of all background processes and their current state(sleeping/running etc.)

- `exec.c` : handles foreground processes using execvp

- `history.txt` : stores 20 previous commands

- `redirection.c` : checks for redirection present, and if yes executes command along with redirection according to arguments given. Allows for both input and output redirection simultaneously.

- `piping.c` : handles multiple pipes along with any redirection present within the pipes by using function inside `redirection.c`.
Allows for all possible combinations of piping and redirection.

- `env.c` : handles setting ( using `setenv` ) and unsetting ( using `unsetenv` ) of environment variables.

- `kjob.c` : Sends a particular process a signal depending on the parameters, called using `kjob` command.


