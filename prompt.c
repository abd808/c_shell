#include "prompt.h"
#include "headers.h"

int prefix(const char *pre, const char *str)
{
    return strncmp(pre, str, strlen(pre)) == 0;
}
 
void prompt(char og_dir[400]) {

	char cur_dir[200];
	char* usr_name;
	char* sys_name;
	char* final_message;
	getcwd(cur_dir,sizeof(cur_dir));
	usr_name=(char*) malloc(200*sizeof(char));   // getting username
	sys_name=(char*) malloc(200*sizeof(char));		// getting system name
	final_message=(char*) malloc(300*sizeof(char));
	usr_name=getlogin();
	gethostname(sys_name,200);
	//printf("%s",sys_name);
	if(prefix(og_dir,cur_dir)){
		char home_path[200];
		home_path[0]='~';
		int j=1;
		for (int i=strlen(og_dir);i<strlen(cur_dir);i++)
			home_path[j++]=cur_dir[i];                                        // replaceing home directory address with ~
		home_path[j]='\0';
		
		sprintf(final_message,"<%s@%s:%s> ",usr_name,sys_name,home_path);    // final prompt
		printf("\033[1;34m");
		printf("%s",final_message);
		printf("\033[0m");
		if(lwd_flag==0){
			strcpy(lwd,home_path);
			strcpy(lwd2,home_path);
			lwd_flag++;
		}
		else if(strcmp(lwd2,home_path)!=0)
		{
			strcpy(lwd,lwd2);
			strcpy(lwd2,home_path);
		}
		
	}
	else{
    sprintf(final_message,"<%s@%s:%s> ",usr_name,sys_name,cur_dir);  
    printf("\033[1;34m");
    printf("%s",final_message);
    printf("\033[0m");
	if(lwd_flag==0){
			strcpy(lwd,cur_dir);
			strcpy(lwd2,cur_dir);
		}
		else if(strcmp(lwd2,cur_dir)!=0)
		{
			strcpy(lwd,lwd2);
			strcpy(lwd2,cur_dir);
		}
}

}
