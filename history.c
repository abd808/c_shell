#include "headers.h"
#include "history.h"

char filename[200];
char his[21][200];
int his_no=0;
void load_history(char og_dir[100])
{
	sprintf(filename,"%s/history.txt",og_dir);

	FILE *f=fopen(filename,"r");
	if(!(f==NULL))
	{
		char transfer[190];			//variable to act as a buffer
		while(fgets(transfer,sizeof(transfer),f)!=NULL)
		{
			his_no+=1;
			strcpy(his[his_no],transfer);
			for (int i=1;i<strlen(his[his_no]);i++)
				if(his[his_no][i]=='\n')
				{
					his[his_no][i]='\0';
					break;
				}
		}
	}
	fclose(f);
}
void update_history(char *command)
{
	if(his_no==0)
	{
		strcpy(his[1],command);
		his_no++;
	}
	else if(his_no==1)
	{
		strcpy(his[2],his[1]);
		strcpy(his[1],command);
		his_no++;
	}
	else 
	{
		for(int i=his_no;i>=1;i--)
		{
			strcpy(his[i+1],his[i]);
		}
		strcpy(his[1],command);
		if(his_no!=20)
			his_no+=1;
	}

	FILE *g;
	g=fopen(filename,"w");
	for (int i=1;i<=his_no;i++)
		fprintf(g, "%s\n", his[i]);
	fclose(g);
}
void dis_history(char *command)
{
	char inputs[100][100];
    int input_count=0;
    char *token = strtok(command," \t");
    while (token!=NULL)
    {
    	strcpy(inputs[input_count],token);
        	input_count++;
        	token=strtok(NULL," \t");  
    }


    if(input_count>2){
    	printf("Too many arguments\n");
    	return;
    }
 
    if(input_count==1){
    	
    	int limit;
    	if(his_no>=10)
    		limit=10;
    	else
    		limit=his_no;
		for (int i=limit;i>=1;i--)
			printf("%s\n", his[i]);
	}
	else
	{
		int limit=atoi(inputs[1]);
		if(limit>20){
			limit=20;
			printf("\nArgument cannot be greater than 20\n\n");
		}
		if(his_no<20)
			limit=his_no;
		for (int i=limit;i>=1;i--)
			printf("%s\n", his[i]);
	}
}