#include "headers.h"
#include "pinfo.h"

void pinfo(char* command)
{
    char inputs[100][100];
    int input_count=0;
    char *token = strtok(command," \t");
    while (token!=NULL)
    {
        strcpy(inputs[input_count],token);
            //printf("tok|%s|\n",inputs[input_count]);
            input_count++;
            token=strtok(NULL," \t");  
    }

    if(input_count>2){
        printf("Too many arguments\n");
        return;
    }

    int pid;

    if(input_count==1) //information about running process
    {
         pid =getpid();
        printf("pid -- %d\n",pid);  
    }
    else
    {
        printf("pid -- %s\n",inputs[1]);
        sscanf(inputs[1],"%d",&pid);
        struct stat sts;
        char check[100];
        sprintf(check,"/proc/%d",pid);
        if(stat(check,&sts)==-1 && errno == ENOENT)
        {
            printf("Process Doesnt Exist\n");
            return;
        }
    }

    char filename[100];
    sprintf(filename,"/proc/%d/stat",pid);
    char vm[100];

    FILE *f=fopen(filename,"r");
    FILE *g=fopen(filename,"r");

     int unused;

     char comm[100],state;
     char* ex_path;

     ex_path=(char*) malloc(100*sizeof(char));
     
     fscanf(f,"%d %s %c ",&unused,comm,&state);

     for (int i=0;i<=22;i++)
     {
        fscanf(g,"%s",vm); // getting 23rd value from proc/stat
        //printf("%s\n",vm );
     }
     
   
     printf("Process Status -- %c\n",state);
     printf("memory -- %s\n",vm );
     printf("Executable Path -- ");

     // getting exec path
     char filename1[100];
     sprintf(filename1,"/proc/%d/exe",pid);
     int len=readlink(filename1, ex_path, 100);
        ex_path[len]='\0';
     for (int i=0;i<strlen(ex_path);i++)
     {
        if(ex_path[i]!='(' && ex_path[i]!=')')
            printf("%c",ex_path[i]);
     }
     printf("\n");

    return;
}